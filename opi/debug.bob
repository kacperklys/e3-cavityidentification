<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>Debug</name>
  <width>740</width>
  <height>930</height>
  <widget type="rectangle" version="2.0.0">
    <name>Titlebar</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>740</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <class>TITLE</class>
    <text>Cavity Identification</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>360</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Waveforms_group</name>
    <x>20</x>
    <y>70</y>
    <width>700</width>
    <height>840</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <widget type="rectangle" version="2.0.0">
      <name>Waveforms_rectangle</name>
      <width>700</width>
      <height>840</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Waveforms_title</name>
      <class>HEADER2</class>
      <text>Waveforms From LLRF IOC</text>
      <width>700</width>
      <height>40</height>
      <font use_class="true">
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <background_color>
        <color name="RED-BORDER" red="150" green="8" blue="16">
        </color>
      </background_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="xyplot" version="2.0.0">
      <name>Amplitude_plot</name>
      <x>20</x>
      <y>90</y>
      <width>320</width>
      <height>210</height>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <x_axis>
        <title>Time [ms]</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </x_axis>
      <y_axes>
        <y_axis>
          <title>Magnitude [-]</title>
          <autoscale>true</autoscale>
          <log_scale>false</log_scale>
          <minimum>0.0</minimum>
          <maximum>100.0</maximum>
          <show_grid>false</show_grid>
          <title_font>
            <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
            </font>
          </title_font>
          <scale_font>
            <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
            </font>
          </scale_font>
          <visible>true</visible>
        </y_axis>
      </y_axes>
      <traces>
        <trace>
          <name>Cavity Voltage Magnitude</name>
          <x_pv>$(P)$(R)Dwn0XAxis-RB</x_pv>
          <y_pv>$(P)$(R)Dwn0Cmp0-RB</y_pv>
          <err_pv></err_pv>
          <axis>0</axis>
          <trace_type>1</trace_type>
          <color>
            <color red="0" green="0" blue="255">
            </color>
          </color>
          <line_width>1</line_width>
          <line_style>0</line_style>
          <point_type>0</point_type>
          <point_size>10</point_size>
          <visible>true</visible>
        </trace>
      </traces>
    </widget>
    <widget type="xyplot" version="2.0.0">
      <name>Phase_plot</name>
      <x>360</x>
      <y>90</y>
      <width>320</width>
      <height>210</height>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <x_axis>
        <title>Time [ms]</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </x_axis>
      <y_axes>
        <y_axis>
          <title>Phase [rad]</title>
          <autoscale>true</autoscale>
          <log_scale>false</log_scale>
          <minimum>0.0</minimum>
          <maximum>100.0</maximum>
          <show_grid>false</show_grid>
          <title_font>
            <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
            </font>
          </title_font>
          <scale_font>
            <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
            </font>
          </scale_font>
          <visible>true</visible>
        </y_axis>
      </y_axes>
      <traces>
        <trace>
          <name>Cavity Voltage Phase</name>
          <x_pv>$(P)$(R)Dwn0XAxis-RB</x_pv>
          <y_pv>$(P)$(R)Dwn0Cmp1-RB</y_pv>
          <err_pv></err_pv>
          <axis>0</axis>
          <trace_type>1</trace_type>
          <color>
            <color red="0" green="0" blue="255">
            </color>
          </color>
          <line_width>1</line_width>
          <line_style>0</line_style>
          <point_type>0</point_type>
          <point_size>10</point_size>
          <visible>true</visible>
        </trace>
      </traces>
    </widget>
    <widget type="xyplot" version="2.0.0">
      <name>VfAmplitude_plot</name>
      <x>20</x>
      <y>350</y>
      <width>320</width>
      <height>210</height>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <x_axis>
        <title>Time [ms]</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </x_axis>
      <y_axes>
        <y_axis>
          <title>Magnitude [-]</title>
          <autoscale>true</autoscale>
          <log_scale>false</log_scale>
          <minimum>0.0</minimum>
          <maximum>100.0</maximum>
          <show_grid>false</show_grid>
          <title_font>
            <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
            </font>
          </title_font>
          <scale_font>
            <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
            </font>
          </scale_font>
          <visible>true</visible>
        </y_axis>
      </y_axes>
      <traces>
        <trace>
          <name>Forward Voltage Magnitude</name>
          <x_pv>$(P)$(R)Dwn5XAxis-RB</x_pv>
          <y_pv>$(P)$(R)Dwn5Cmp0-RB</y_pv>
          <err_pv></err_pv>
          <axis>0</axis>
          <trace_type>1</trace_type>
          <color>
            <color red="0" green="0" blue="255">
            </color>
          </color>
          <line_width>1</line_width>
          <line_style>0</line_style>
          <point_type>0</point_type>
          <point_size>10</point_size>
          <visible>true</visible>
        </trace>
      </traces>
    </widget>
    <widget type="xyplot" version="2.0.0">
      <name>VfPhase_plot</name>
      <x>360</x>
      <y>350</y>
      <width>320</width>
      <height>210</height>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <x_axis>
        <title>Time [ms]</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </x_axis>
      <y_axes>
        <y_axis>
          <title>Phase [rad]</title>
          <autoscale>true</autoscale>
          <log_scale>false</log_scale>
          <minimum>0.0</minimum>
          <maximum>100.0</maximum>
          <show_grid>false</show_grid>
          <title_font>
            <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
            </font>
          </title_font>
          <scale_font>
            <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
            </font>
          </scale_font>
          <visible>true</visible>
        </y_axis>
      </y_axes>
      <traces>
        <trace>
          <name>Forward Voltage Phase</name>
          <x_pv>$(P)$(R)Dwn5XAxis-RB</x_pv>
          <y_pv>$(P)$(R)Dwn5Cmp1-RB</y_pv>
          <err_pv></err_pv>
          <axis>0</axis>
          <trace_type>1</trace_type>
          <color>
            <color red="0" green="0" blue="255">
            </color>
          </color>
          <line_width>1</line_width>
          <line_style>0</line_style>
          <point_type>0</point_type>
          <point_size>10</point_size>
          <visible>true</visible>
        </trace>
      </traces>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Amplitude_label</name>
      <class>HEADER3</class>
      <text>Cavity Voltage - Amplitude</text>
      <x>100</x>
      <y>50</y>
      <width>230</width>
      <height>30</height>
      <font use_class="true">
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Phase_label</name>
      <class>HEADER3</class>
      <text>Cavity Voltage - Phase</text>
      <x>450</x>
      <y>50</y>
      <width>200</width>
      <height>30</height>
      <font use_class="true">
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>VfPhase_label</name>
      <class>HEADER3</class>
      <text>Forward Voltage - Phase</text>
      <x>440</x>
      <y>320</y>
      <width>200</width>
      <height>30</height>
      <font use_class="true">
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>VfAmplitude_label</name>
      <class>HEADER3</class>
      <text>Forward Voltage - Amplitude</text>
      <x>90</x>
      <y>320</y>
      <width>230</width>
      <height>30</height>
      <font use_class="true">
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
    </widget>
    <widget type="xyplot" version="2.0.0">
      <name>VrAmplitude_plot</name>
      <x>20</x>
      <y>610</y>
      <width>320</width>
      <height>210</height>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <x_axis>
        <title>Time [ms]</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </x_axis>
      <y_axes>
        <y_axis>
          <title>Magnitude [-]</title>
          <autoscale>true</autoscale>
          <log_scale>false</log_scale>
          <minimum>0.0</minimum>
          <maximum>100.0</maximum>
          <show_grid>false</show_grid>
          <title_font>
            <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
            </font>
          </title_font>
          <scale_font>
            <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
            </font>
          </scale_font>
          <visible>true</visible>
        </y_axis>
      </y_axes>
      <traces>
        <trace>
          <name>Reflected Voltage Magnitude</name>
          <x_pv>$(P)$(R)Dwn6XAxis-RB</x_pv>
          <y_pv>$(P)$(R)Dwn6Cmp0-RB</y_pv>
          <err_pv></err_pv>
          <axis>0</axis>
          <trace_type>1</trace_type>
          <color>
            <color red="0" green="0" blue="255">
            </color>
          </color>
          <line_width>1</line_width>
          <line_style>0</line_style>
          <point_type>0</point_type>
          <point_size>10</point_size>
          <visible>true</visible>
        </trace>
      </traces>
    </widget>
    <widget type="xyplot" version="2.0.0">
      <name>VrPhase_plot</name>
      <x>360</x>
      <y>610</y>
      <width>320</width>
      <height>210</height>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <x_axis>
        <title>Time [ms]</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </x_axis>
      <y_axes>
        <y_axis>
          <title>Phase [rad]</title>
          <autoscale>true</autoscale>
          <log_scale>false</log_scale>
          <minimum>0.0</minimum>
          <maximum>100.0</maximum>
          <show_grid>false</show_grid>
          <title_font>
            <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
            </font>
          </title_font>
          <scale_font>
            <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
            </font>
          </scale_font>
          <visible>true</visible>
        </y_axis>
      </y_axes>
      <traces>
        <trace>
          <name>Reflected Voltage Phase</name>
          <x_pv>$(P)$(R)Dwn6XAxis-RB</x_pv>
          <y_pv>$(P)$(R)Dwn6Cmp1-RB</y_pv>
          <err_pv></err_pv>
          <axis>0</axis>
          <trace_type>1</trace_type>
          <color>
            <color red="0" green="0" blue="255">
            </color>
          </color>
          <line_width>1</line_width>
          <line_style>0</line_style>
          <point_type>0</point_type>
          <point_size>10</point_size>
          <visible>true</visible>
        </trace>
      </traces>
    </widget>
    <widget type="label" version="2.0.0">
      <name>VrPhase_label</name>
      <class>HEADER3</class>
      <text>Reflected Voltage - Phase</text>
      <x>440</x>
      <y>580</y>
      <width>200</width>
      <height>30</height>
      <font use_class="true">
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>VrAmplitude_label</name>
      <class>HEADER3</class>
      <text>Reflected Voltage - Amplitude</text>
      <x>90</x>
      <y>580</y>
      <width>230</width>
      <height>30</height>
      <font use_class="true">
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
    </widget>
  </widget>
  <widget type="label" version="2.0.0">
    <name>subtitle</name>
    <class>SUBTITLE</class>
    <text>Debug Window</text>
    <x>470</x>
    <y use_class="true">20</y>
    <width>250</width>
    <height use_class="true">30</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">2</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
</display>
