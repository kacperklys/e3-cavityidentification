#!/usr/bin/env bash

#caget 1:2:Dwn0Cmp0-RB -t > cavityRespAmp.txt
#caget 1:2:Dwn0Cmp1-RB -t > cavityRespPhase.txt # deg
#caget 1:2:Dwn5Cmp0-RB -t > cavityForwardVoltageAmp.txt
#caget 1:2:Dwn5Cmp1-RB -t > cavityForwardVoltagePhase.txt # deg
#caget 1:2:Dwn6Cmp0-RB -t > cavityReflectedVoltageAmp.txt
#caget 1:2:Dwn6Cmp1-RB -t > cavityReflectedVoltagePhase.txt # deg
#caget 1:2:Dwn0XAxis-RB -t > cavityTime.txt


caget TS2-010RFC:RFS-DIG-101:Dwn0-Cmp0 -t > cavityRespAmp.txt
caget TS2-010RFC:RFS-DIG-101:Dwn0-Cmp1 -t > cavityRespPhase.txt # deg
caget TS2-010RFC:RFS-DIG-101:Dwn5-Cmp0 -t > cavityForwardVoltageAmp.txt
caget TS2-010RFC:RFS-DIG-101:Dwn5-Cmp1 -t > cavityForwardVoltagePhase.txt # deg
caget TS2-010RFC:RFS-DIG-101:Dwn6-Cmp0 -t > cavityReflectedVoltageAmp.txt
caget TS2-010RFC:RFS-DIG-101:Dwn6-Cmp1 -t > cavityReflectedVoltagePhase.txt # deg
caget TS2-010RFC:RFS-DIG-101:Dwn0-XAxis -t > cavityTime.txt


files=("cavityRespAmp.txt" "cavityRespPhase.txt" "cavityForwardVoltageAmp.txt" "cavityForwardVoltagePhase.txt" "cavityReflectedVoltageAmp.txt" "cavityReflectedVoltagePhase.txt" "cavityTime.txt")

# Loop through the list of files
for file in "${files[@]}"; do
  # Check if the file exists
  if [ ! -f "$file" ]; then
    echo "File $file does not exist. Skipping..."
    continue
  fi

  # Read the content of the file
  content=$(cat "$file")

  # Remove the first number (until the first space)
  new_content=$(echo "$content" | sed 's/^[^ ]* //')

  # Overwrite the file with the modified content
  echo "$new_content" > "$file"

  echo "First number removed from $file."
done

./cavityApp -n cavityTime.txt -o cavityRespAmp.txt -p cavityRespPhase.txt -q cavityForwardVoltageAmp.txt -r cavityForwardVoltagePhase.txt -t cavityReflectedVoltageAmp.txt -u cavityReflectedVoltagePhase.txt
