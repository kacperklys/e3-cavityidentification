# Single value readouts
record(ai, "$(P)$(R)SystemFreq-RB") {
    field(DESC, "System Frequency")
    field(EGU,  "MHz")
    field(INP,  "$(LLRF_SYSTEM)FreqSystem CP")
    field(PREC, "3")
}

record(ai, "$(P)$(R)SystemSamplingFreq-RB") {
    field(DESC, "Sampling frequency")
    field(EGU,  "MHz")
    field(INP,  "$(LLRF_SYSTEM)FreqSampling CP")
    field(PREC, "3")
}

record(ai, "$(P)$(R)RFEndPulse-RB") {
    field(DESC, "End of RF pulse")
    field(EGU,  "ms")
    field(INP,  "$(LLRF_PREFIX)PosRFEnd CP")
    field(PREC, "4")
}

record(ai, "$(P)$(R)PosBeamStart-RB") {
    field(DESC, "Beam start position")
    field(EGU,  "ms")
    field(INP,  "$(LLRF_PREFIX)PosBeamStart CP")
    field(PREC, "4")
}

record(ai, "$(P)$(R)PosBeamEnd-RB") {
    field(DESC, "Beam end position")
    field(EGU,  "ms")
    field(INP,  "$(LLRF_PREFIX)PosBeamEnd CP")
    field(PREC, "4")
}

record(ai, "$(P)$(R)PulseWidth-RB") {
    field(DESC, "Pulse width")
    field(EGU,  "us")
    field(INP,  "$(LLRF_EVR)BeamPulseWdt-SP CP")
    field(PREC, "4")
}

record(dfanout, "$(P)$(R)#SystemFreqCopy") {
    field(DESC, "Copy system freq to asyn")
    field(DOL,  "$(P)$(R)SystemFreq-RB CP")
    field(OMSL, "closed_loop")
    field(OUTA, "$(P)$(R)#SystemFreqToAsyn PP")
}

record(dfanout, "$(P)$(R)#SystemSaplingFreqCopy") {
    field(DESC, "Copy sampling freq to asyn")
    field(DOL,  "$(P)$(R)SystemSamplingFreq-RB CP")
    field(OMSL, "closed_loop")
    field(OUTA, "$(P)$(R)#SystemSamplingFreqToAsyn PP")
}

record(dfanout, "$(P)$(R)#RFEndPulseCopy") {
    field(DESC, "Copy rf end pulse to asyn")
    field(DOL,  "$(P)$(R)RFEndPulse-RB CP")
    field(OMSL, "closed_loop")
    field(OUTA, "$(P)$(R)#RFEndPulseToAsyn PP")
}

record(ao, "$(P)$(R)#SystemFreqToAsyn") {
    field(DESC, "System freq in asyn")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))SYS_FREQ")
    field(EGU,  "MHz")
}

record(ao, "$(P)$(R)#SystemSamplingFreqToAsyn") {
    field(DESC, "Sampling freq in asyn")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))SYS_SAMP_FREQ")
    field(EGU,  "MHz")
}

record(ao, "$(P)$(R)#RFEndPulseToAsyn") {
    field(DESC, "rf pulse end in asyn")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))RF_END")
    field(EGU,  "ms")
}

record (dfanout, "$(P)$(R)#PosBeamStartCopy") {
    field(DESC, "Beam start copy to asyn")
    field(DOL,  "$(P)$(R)PosBeamStart-RB CP")
    field(OMSL, "closed_loop")
    field(OUTA, "$(P)$(R)#PosBeamStartToAsyn PP")
}

record(ao, "$(P)$(R)#PosBeamStartToAsyn") {
    field(DESC, "Beam start in asyn")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))BEAM_START")
    field(EGU,  "ms")
}

record (dfanout, "$(P)$(R)#PosBeamEndCopy") {
    field(DESC, "Beam end copy to asyn")
    field(DOL,  "$(P)$(R)PosBeamEnd-RB CP")
    field(OMSL, "closed_loop")
    field(OUTA, "$(P)$(R)#PosBeamEndToAsyn PP")
}

record(ao, "$(P)$(R)#PosBeamEndToAsyn") {
    field(DESC, "Beam end in asyn")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))BEAM_END")
    field(EGU,  "ms")
}

record(dfanout, "$(P)$(R)#PulseWidthCopy") {
    field(DESC, "Pulse width copy to asyn")
    field(DOL,  "$(P)$(R)PulseWidth-RB CP")
    field(OMSL, "closed_loop")
    field(OUTA, "$(P)$(R)#PulseWidthToAsyn PP")
}

record(ao, "$(P)$(R)#PulseWidthToAsyn") {
    field(DESC, "Pulse width in asyn")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))PULSE_WIDTH")
    field(EGU,  "us")
}
