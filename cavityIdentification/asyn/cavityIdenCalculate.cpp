#include "cavityIdenAsynPortDriver.h"
#include <stdlib.h>
#include <cstring>

#ifndef M_PI
  #define M_PI 3.14159265358979323846
#endif

#define CAVITY_AMP_TOO_LOW 1
#define CAVITY_AMP_OK 0


using namespace std;


extern "C" {
  void iocPrint(const char *format, ...);
}

void cavityIdenCalculate(void *drvPvt)
{
  cavityIdenAsynPortDriver *pPvt = (cavityIdenAsynPortDriver *)drvPvt;
  pPvt -> cavityIdenCalculate();
}

void cavityIdenAsynPortDriver::cavityIdenCalculate(void)
{
  epicsMutexMustLock(terminateWorkersLock);
  iocPrint("Starting Calculation Thread\n");
  while(terminateWorkers == 0)
  {
    epicsMutexUnlock(terminateWorkersLock);
    if(recalculate)
    {
      if(cav.vcAmpRawAvg >= ampMinLevel)
      {
        // normalise waveforms
        cav.init();
        // get preprocessed waveforms
        doCallbacksFloat64Array(cav.vcAmp, cav.downMaxSize, P_VC_AMP_WAVE_RB, 0);
        doCallbacksFloat64Array(cav.vcPhsDeg, cav.downMaxSize, P_VC_PHS_WAVE_RB, 0);
        doCallbacksFloat64Array(cav.vfAmp, cav.downMaxSize, P_VF_AMP_WAVE_RB, 0);
        doCallbacksFloat64Array(cav.vfPhsDeg, cav.downMaxSize, P_VF_PHS_WAVE_RB, 0);
        doCallbacksFloat64Array(cav.vrAmp, cav.downMaxSize, P_VR_AMP_WAVE_RB, 0);
        doCallbacksFloat64Array(cav.vrPhsDeg, cav.downMaxSize, P_VR_PHS_WAVE_RB, 0);
        // find decay of cavity voltage
        size_t idxs[2];
        double amps[2];
        double timeDecay = cav.findTimeDecay(idxs, amps);
        // caluclate Q from decay
        double qlDecay = cav.QlFromDecay(timeDecay);
        // calculate detuning
        double alfaDetu = cav.findDetuningSlope(idxs);
        // calculate Q from logarithm decay slope
        double w12Ext = cav.findLogarithmSlope(idxs);
        lock();
        setDoubleParam(P_DECAY_RB, timeDecay);
        setDoubleParam(P_QL_RB, qlDecay);
        setDoubleParam(P_DETU_DECAY_RB, alfaDetu/(2 * M_PI));
        setDoubleParam(P_QL_LOG_RB, cav.QlFromLogarithm(w12Ext));
        unlock();

        // calibrate cavity forward and cavity reflected signals
        cav.calibrateVfandVr(alfaDetu);
        // calibrated vc, vf and vr
        double *vfCalAmp=NULL, *vfCalPhs=NULL, *vrCalAmp=NULL, *vrCalPhs=NULL;
        doCallbacksFloat64Array(cav.vcAmpCal, cav.downMaxSize, P_VC_AMP_CAL_WAVE_RB, 0);
        doCallbacksFloat64Array(cav.vcPhsCalDeg, cav.downMaxSize, P_VC_PHS_CAL_WAVE_RB, 0);
        if(cav.vfVrCalState)
        {
          vfCalAmp = cav.vfAmpCal;
          vfCalPhs = cav.vfPhsCalDeg;
          vrCalAmp = cav.vrAmpCal;
          vrCalPhs = cav.vrPhsCalDeg;
        }
        else
        {
          vfCalAmp = cav.vfAmp;
          vfCalPhs = cav.vfPhsDeg;
          vrCalAmp = cav.vrAmp;
          vrCalPhs = cav.vrPhsDeg;
        }
        doCallbacksFloat64Array(vfCalAmp, cav.downMaxSize, P_VF_AMP_CAL_WAVE_RB, 0);
        doCallbacksFloat64Array(vfCalPhs, cav.downMaxSize, P_VF_PHS_CAL_WAVE_RB, 0);
        doCallbacksFloat64Array(vrCalAmp, cav.downMaxSize, P_VR_AMP_CAL_WAVE_RB, 0);
        doCallbacksFloat64Array(vrCalPhs, cav.downMaxSize, P_VR_PHS_CAL_WAVE_RB, 0);
        // load window waveforms
        generateWindows(cav.vcAmpCal, cav.vcPhsCalDeg, cav.downMaxSize, idxs);
        // find max gradient
        double max = cav.maxGradient();
        // calculate detuning and Q over time using
        size_t n = 0, idx300us = 0;
        cav.calculateQandDetuOverTime(w12Ext, &n, &idx300us);
        doCallbacksFloat64Array(cav.q, n, P_QL_WAVE_RB, 0);
        doCallbacksFloat64Array(cav.detu, cav.downMaxSize, P_DETU_WAVE_RB, 0);
        doCallbacksFloat64Array(cav.vcX + idx300us, n, P_Q_DETU_X_RB, 0);
        // calculate detunig at RF pulse end
        lock();
        setDoubleParam(P_DETU_PULSE_END_RB, cav.calcDetuPulseEnd(n));
        setDoubleParam(P_MAX_GRAD_RB, max);
        setDoubleParam(P_VC_AMP_CAL_RB, cav.vcScale);
        setDoubleParam(P_VC_PHS_CAL_RB, cav.vcRot);
        setDoubleParam(P_VF_AMP_CAL_RB, cav.vfScale);
        setDoubleParam(P_VF_PHS_CAL_RB, cav.vfRot);
        setDoubleParam(P_VR_AMP_CAL_RB, cav.vrScale);
        setDoubleParam(P_VR_PHS_CAL_RB, cav.vrRot);
        setIntegerParam(P_ERROR_MSG_RB, CAVITY_AMP_OK);
        unlock();
      }
      else
      {
        lock();
        setIntegerParam(P_ERROR_MSG_RB, CAVITY_AMP_TOO_LOW);
        // clear cavity params when error
        setDoubleParam(P_DECAY_RB, 0.0);
        setDoubleParam(P_QL_RB, 0.0);
        setDoubleParam(P_DETU_DECAY_RB, 0.0);
        setDoubleParam(P_DETU_PULSE_END_RB, 0.0);
        setDoubleParam(P_QL_LOG_RB, 0.0);
        setDoubleParam(P_MAX_GRAD_RB, 0.0);
        unlock();
      }
      recalculate = false;
    }
    callParamCallbacks();
    epicsMutexMustLock(terminateWorkersLock);
  }
  epicsMutexUnlock(terminateWorkersLock);
  // termination handling
  pthread_mutex_lock(&ackFromWorkersLock);
  ++ackFromWorkersCount;
  pthread_cond_signal(&ackFromWorkersCond);
  pthread_mutex_unlock(&ackFromWorkersLock);
}
