#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <sys/time.h>
#include <iostream>

using namespace std ;

#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <epicsMutex.h>
#include <errlog.h>
#include <iocsh.h>

#include "cavityIdenAsynPortDriver.h"
#include <epicsExport.h>


// flattop calibration coefficients
const char *P_VC_AMP_CAL_RB_String = 		"VC_AMP_CAL_RB";
const char *P_VC_PHS_CAL_RB_String = 		"VC_PHS_CAL_RB";
const char *P_VF_AMP_CAL_RB_String = 		"VF_AMP_CAL_RB";
const char *P_VF_PHS_CAL_RB_String = 		"VF_PHS_CAL_RB";
const char *P_VR_AMP_CAL_RB_String = 		"VR_AMP_CAL_RB";
const char *P_VR_PHS_CAL_RB_String = 		"VR_PHS_CAL_RB";
const char *P_VC_AMP_CAL_SP_String = 		"VC_AMP_CAL_SP";
const char *P_VC_PHS_CAL_SP_String =		"VC_PHS_CAL_SP";
const char *P_VF_AMP_CAL_SP_String = 		"VF_AMP_CAL_SP";
const char *P_VF_PHS_CAL_SP_String = 		"VF_PHS_CAL_SP";
const char *P_VR_AMP_CAL_SP_String = 		"VR_AMP_CAL_SP";
const char *P_VR_PHS_CAL_SP_String = 		"VR_PHS_CAL_SP";
// min cavity amp level
const char *P_VC_AMP_MIN_SP_String =        "VC_AMP_MIN_SP";
// cavity calculated parameters
const char *P_MAX_GRAD_RB_String = 	    	"MAX_GRAD_RB";
const char *P_DETU_DECAY_RB_String = 		"DETU_DECAY_RB";
const char *P_DETU_PULSE_END_RB_String = 	"DETU_PULSE_END_RB";
const char *P_QL_RB_String = 		    	"QL_RB";
const char *P_QL_LOG_RB_String = 	    	"QL_LOG_RB";
const char *P_DECAY_RB_String = 	    	"DECAY_RB";
// calculated waveforms
const char *P_QL_WAVE_RB_String = 	    	"QL_WAVE_RB";
const char *P_DETU_WAVE_RB_String = 		"DETU_WAVE_RB";
const char *P_Q_DETU_X_RB_String = 	    	"Q_DETU_X_RB";
const char *P_VC_PHS_WAVE_RB_String = 		"VC_PHS_WAVE_RB";
const char *P_VC_AMP_WAVE_RB_String = 		"VC_AMP_WAVE_RB";
const char *P_VF_PHS_WAVE_RB_String = 		"VF_PHS_WAVE_RB";
const char *P_VF_AMP_WAVE_RB_String = 		"VF_AMP_WAVE_RB";
const char *P_VR_PHS_WAVE_RB_String = 		"VR_PHS_WAVE_RB";
const char *P_VR_AMP_WAVE_RB_String = 		"VR_AMP_WAVE_RB";
const char *P_VC_AMP_CAL_WAVE_RB_String = 	"VC_AMP_CAL_WAVE_RB";
const char *P_VC_PHS_CAL_WAVE_RB_String = 	"VC_PHS_CAL_WAVE_RB";
const char *P_VF_AMP_CAL_WAVE_RB_String = 	"VF_AMP_CAL_WAVE_RB";
const char *P_VF_PHS_CAL_WAVE_RB_String = 	"VF_PHS_CAL_WAVE_RB";
const char *P_VR_AMP_CAL_WAVE_RB_String = 	"VR_AMP_CAL_WAVE_RB";
const char *P_VR_PHS_CAL_WAVE_RB_String = 	"VR_PHS_CAL_WAVE_RB";
// waveforms from LLRF IOC
const char *P_DWN0_CMP0_String = 	    	"DWN0_CMP0";
const char *P_DWN5_CMP0_String = 	    	"DWN5_CMP0";
const char *P_DWN6_CMP0_String = 	    	"DWN6_CMP0";
const char *P_DWN0_CMP1_String = 	    	"DWN0_CMP1";
const char *P_DWN5_CMP1_String = 	    	"DWN5_CMP1";
const char *P_DWN6_CMP1_String = 	    	"DWN6_CMP1";
const char *P_DWN0_XAXIS_String = 	    	"DWN0_XAXIS";
const char *P_DWN5_XAXIS_String = 	    	"DWN5_XAXIS";
const char *P_DWN6_XAXIS_String = 	    	"DWN6_XAXIS";
// system info from LLRF IOC
const char *P_SYS_FREQ_String = 		    "SYS_FREQ";
const char *P_SYS_SAMP_FREQ_String = 		"SYS_SAMP_FREQ";
const char *P_RF_END_String = 		    	"RF_END";
// window
const char *P_QL_AMP_WINDOW_RB_String = 	"QL_AMP_WINDOW_RB";
const char *P_QL_PHS_WINDOW_RB_String = 	"QL_PHS_WINDOW_RB";
// offset
const char *P_QL_OFFSET_RB_String = 		"QL_OFFSET_RB";
const char *P_QL_OFFSET_SP_String = 		"QL_OFFSET_SP";
// custom flattop calibration
const char *P_CUSTOM_COEFFS_SP_String = 	"CUSTOM_COEFFS_SP";
const char *P_CUSTOM_CALIBRATE_SP_String = 	"CUSTOM_CALIBRATE_SP";
// Vr Vf calibration
const char *P_CALIBRATION_STATE_SP_String = "CALIBRATION_STATE_SP";
// beam params from LLRF IOC
const char *P_BEAM_START_String = 		    "BEAM_START";
const char *P_BEAM_END_String = 		    "BEAM_END";
const char *P_PULSE_WIDTH_String = 		    "PULSE_WIDTH";
// error messages
const char *P_ERROR_MSG_RB_String =         "ERROR_MSG_RB";


extern "C" {
  void iocPrint(const char *format, ...);
}


static const char *driverName = "cavityIdenAsynPortDriverPort";
void cavityIdenCalculate(void *drvPvt);


cavityIdenAsynPortDriver::cavityIdenAsynPortDriver(const char *portName,  size_t samplesNum, const char *devName)
  :asynPortDriver(portName,
                  1,
                  asynInt32Mask | asynFloat64Mask | asynFloat64ArrayMask | asynDrvUserMask,
                  asynInt32Mask | asynFloat64Mask | asynFloat64ArrayMask,
                  0,
                  1,
                  0,
                  0)
  , cav(samplesNum, &iocPrint)
{
  asynStatus status;
  const char *functionName = "cavityIdenAsynPortDriver";

  // create asyn params
  // flattop calibration coefficients
  createParam(P_VC_AMP_CAL_RB_String, 		    asynParamFloat64, 	    &P_VC_AMP_CAL_RB);
  createParam(P_VC_PHS_CAL_RB_String, 		    asynParamFloat64, 	    &P_VC_PHS_CAL_RB);
  createParam(P_VF_AMP_CAL_RB_String, 		    asynParamFloat64, 	    &P_VF_AMP_CAL_RB);
  createParam(P_VF_PHS_CAL_RB_String, 		    asynParamFloat64, 	    &P_VF_PHS_CAL_RB);
  createParam(P_VR_AMP_CAL_RB_String, 		    asynParamFloat64, 	    &P_VR_AMP_CAL_RB);
  createParam(P_VR_PHS_CAL_RB_String, 		    asynParamFloat64, 	    &P_VR_PHS_CAL_RB);
  createParam(P_VC_AMP_CAL_SP_String, 		    asynParamFloat64, 	    &P_VC_AMP_CAL_SP);
  createParam(P_VC_PHS_CAL_SP_String, 		    asynParamFloat64, 	    &P_VC_PHS_CAL_SP);
  createParam(P_VF_AMP_CAL_SP_String, 		    asynParamFloat64, 	    &P_VF_AMP_CAL_SP);
  createParam(P_VF_PHS_CAL_SP_String, 		    asynParamFloat64, 	    &P_VF_PHS_CAL_SP);
  createParam(P_VR_AMP_CAL_SP_String, 		    asynParamFloat64, 	    &P_VR_AMP_CAL_SP);
  createParam(P_VR_PHS_CAL_SP_String, 		    asynParamFloat64, 	    &P_VR_PHS_CAL_SP);
  // cavity amp min level
  createParam(P_VC_AMP_MIN_SP_String,           asynParamFloat64,       &P_VC_AMP_MIN_SP);
  // cavity parameters
  createParam(P_MAX_GRAD_RB_String, 		    asynParamFloat64, 	    &P_MAX_GRAD_RB);
  createParam(P_DETU_DECAY_RB_String, 		    asynParamFloat64, 	    &P_DETU_DECAY_RB);
  createParam(P_DETU_PULSE_END_RB_String, 	    asynParamFloat64, 	    &P_DETU_PULSE_END_RB);
  createParam(P_QL_RB_String, 			        asynParamFloat64, 	    &P_QL_RB);
  createParam(P_QL_LOG_RB_String, 		        asynParamFloat64, 	    &P_QL_LOG_RB);
  createParam(P_DECAY_RB_String, 		        asynParamFloat64, 	    &P_DECAY_RB);
  // calculated waveforms
  createParam(P_QL_WAVE_RB_String, 		        asynParamFloat64Array, 	&P_QL_WAVE_RB);
  createParam(P_DETU_WAVE_RB_String, 		    asynParamFloat64Array, 	&P_DETU_WAVE_RB);
  createParam(P_Q_DETU_X_RB_String, 		    asynParamFloat64Array, 	&P_Q_DETU_X_RB);
  createParam(P_VC_PHS_WAVE_RB_String, 		    asynParamFloat64Array, 	&P_VC_PHS_WAVE_RB);
  createParam(P_VC_AMP_WAVE_RB_String, 		    asynParamFloat64Array, 	&P_VC_AMP_WAVE_RB);
  createParam(P_VF_PHS_WAVE_RB_String, 		    asynParamFloat64Array, 	&P_VF_PHS_WAVE_RB);
  createParam(P_VF_AMP_WAVE_RB_String, 		    asynParamFloat64Array, 	&P_VF_AMP_WAVE_RB);
  createParam(P_VR_PHS_WAVE_RB_String, 		    asynParamFloat64Array, 	&P_VR_PHS_WAVE_RB);
  createParam(P_VR_AMP_WAVE_RB_String, 		    asynParamFloat64Array, 	&P_VR_AMP_WAVE_RB);
  createParam(P_VC_AMP_CAL_WAVE_RB_String, 	    asynParamFloat64Array, 	&P_VC_AMP_CAL_WAVE_RB);
  createParam(P_VC_PHS_CAL_WAVE_RB_String, 	    asynParamFloat64Array, 	&P_VC_PHS_CAL_WAVE_RB);
  createParam(P_VF_AMP_CAL_WAVE_RB_String, 	    asynParamFloat64Array, 	&P_VF_AMP_CAL_WAVE_RB);
  createParam(P_VF_PHS_CAL_WAVE_RB_String, 	    asynParamFloat64Array, 	&P_VF_PHS_CAL_WAVE_RB);
  createParam(P_VR_AMP_CAL_WAVE_RB_String, 	    asynParamFloat64Array, 	&P_VR_AMP_CAL_WAVE_RB);
  createParam(P_VR_PHS_CAL_WAVE_RB_String, 	    asynParamFloat64Array, 	&P_VR_PHS_CAL_WAVE_RB);
  // waveforms from LLRF IOC
  createParam(P_DWN0_CMP0_String, 		        asynParamFloat64Array, 	&P_DWN0_CMP0);
  createParam(P_DWN5_CMP0_String, 		        asynParamFloat64Array,	&P_DWN5_CMP0);
  createParam(P_DWN6_CMP0_String, 		        asynParamFloat64Array, 	&P_DWN6_CMP0);
  createParam(P_DWN0_CMP1_String, 		        asynParamFloat64Array, 	&P_DWN0_CMP1);
  createParam(P_DWN5_CMP1_String, 		        asynParamFloat64Array, 	&P_DWN5_CMP1);
  createParam(P_DWN6_CMP1_String, 		        asynParamFloat64Array, 	&P_DWN6_CMP1);
  createParam(P_DWN0_XAXIS_String, 		        asynParamFloat64Array, 	&P_DWN0_XAXIS);
  createParam(P_DWN5_XAXIS_String, 		        asynParamFloat64Array, 	&P_DWN5_XAXIS);
  createParam(P_DWN6_XAXIS_String, 		        asynParamFloat64Array, 	&P_DWN6_XAXIS);
  // system info from LLRF IOC
  createParam(P_SYS_FREQ_String, 		        asynParamFloat64, 	    &P_SYS_FREQ);
  createParam(P_SYS_SAMP_FREQ_String, 		    asynParamFloat64,	    &P_SYS_SAMP_FREQ);
  createParam(P_RF_END_String, 			        asynParamFloat64, 	    &P_RF_END);
  // window
  createParam(P_QL_AMP_WINDOW_RB_String, 	    asynParamFloat64Array, 	&P_QL_AMP_WINDOW_RB);
  createParam(P_QL_PHS_WINDOW_RB_String, 	    asynParamFloat64Array, 	&P_QL_PHS_WINDOW_RB);
  // offset
  createParam(P_QL_OFFSET_RB_String, 		    asynParamInt32, 	    &P_QL_OFFSET_RB);
  createParam(P_QL_OFFSET_SP_String, 		    asynParamInt32, 	    &P_QL_OFFSET_SP);
  // custom flattop calibration
  createParam(P_CUSTOM_COEFFS_SP_String, 	    asynParamInt32, 	    &P_CUSTOM_COEFFS_SP);
  createParam(P_CUSTOM_CALIBRATE_SP_String, 	asynParamInt32, 	    &P_CUSTOM_CALIBRATE_SP);
  // Vr Vf calibration
  createParam(P_CALIBRATION_STATE_SP_String,	asynParamInt32, 	    &P_CALIBRATION_STATE_SP);
  // beam params from LLRF IOC
  createParam(P_BEAM_START_String, 		        asynParamFloat64, 	    &P_BEAM_START);
  createParam(P_BEAM_END_String, 		        asynParamFloat64, 	    &P_BEAM_END);
  createParam(P_PULSE_WIDTH_String, 		    asynParamFloat64, 	    &P_PULSE_WIDTH);
  // error messages
  createParam(P_ERROR_MSG_RB_String,            asynParamInt32,         &P_ERROR_MSG_RB);

  // parameters initialisation
  lock();
  setDoubleParam(P_VC_AMP_CAL_SP, 	1.0);
  setDoubleParam(P_VC_PHS_CAL_SP, 	0.0);
  setDoubleParam(P_VF_AMP_CAL_SP, 	1.0);
  setDoubleParam(P_VF_PHS_CAL_SP, 	0.0);
  setDoubleParam(P_VR_AMP_CAL_SP, 	1.0);
  setDoubleParam(P_VR_PHS_CAL_SP, 	0.0);
  setIntegerParam(P_CUSTOM_COEFFS_SP, 	0);
  setIntegerParam(P_CUSTOM_CALIBRATE_SP,0);
  unlock();

  tempCustomCoeff = false;
  callParamCallbacks();

  terminateWorkers = 0;
  terminateWorkersLock = epicsMutexMustCreate();

  ackFromWorkersCount = 0;
  pthread_mutex_init(&ackFromWorkersLock, NULL);
  pthread_cond_init(&ackFromWorkersCond, NULL);

  // IRQ thread
  status = (asynStatus)(epicsThreadCreate("cavityIdenCalculate",
                                          epicsThreadPriorityMedium,
                                          epicsThreadGetStackSize(epicsThreadStackMedium),
                                          (EPICSTHREADFUNC)::cavityIdenCalculate,
                                          this) == NULL);

  if (status)
  {
    errlogPrintf("%s:%s: epicsThreadCreate failure\n", driverName, functionName);
    return;
  }
}

asynStatus cavityIdenAsynPortDriver::writeFloat64(asynUser *pasynUser, epicsFloat64 value)
{
  int function = pasynUser->reason;
  asynStatus status = asynSuccess;
  // cavity amp min level
  if(function == P_VC_AMP_MIN_SP)
  {
    ampMinLevel = value;
    return status;
  }
  // cavity voltage amplitude calibration factor
  if(function == P_VC_AMP_CAL_SP)
  {
    tempVcAmpCoeff = value;
    return status;
  }
  // cavity voltage phase calibration factor
  if(function == P_VC_PHS_CAL_SP)
  {
    tempVcPhsCoeff = value;
    return status;
  }
  // forward voltage amplitude calibration factor
  if(function == P_VF_AMP_CAL_SP)
  {
    tempVfAmpCoeff = value;
    return status;
  }
  // forward voltage phase calibration factor
  if(function == P_VF_PHS_CAL_SP)
  {
    tempVfPhsCoeff = value;
    return status;
  }
  // reflected voltage amplitude calibration factor
  if(function == P_VR_AMP_CAL_SP)
  {
    tempVrAmpCoeff = value;
    return status;
  }
  // reflected voltage phase calibration factor
  if(function == P_VR_PHS_CAL_SP)
  {
    tempVrPhsCoeff = value;
    return status;
  }

  // system frequency
  if(function == P_SYS_FREQ)
  {
    cav.setResFreq(value);
    recalculate = true;
    return status;
  }
  // system sampling frequency
  if(function == P_SYS_SAMP_FREQ)
  {
    system_sampling_freq = value;
    recalculate = true;
    return status;
  }
  // rf pulse end
  if(function == P_RF_END)
  {
    cav.setPulseEnd(value);
    recalculate = true;
    return status;
  }
  // beam start
  if(function == P_BEAM_START)
  {
    cav.setBeamStart(value);
    return status;
  }
  // beam end
  if(function == P_BEAM_END)
  {
    cav.setBeamEnd(value);
    return status;
  }
  // pulse width
  if(function == P_PULSE_WIDTH)
  {
    cav.setPulseWidth(value);
    return status;
  }
  return status;
}

asynStatus cavityIdenAsynPortDriver::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
  int function = pasynUser->reason;
  asynStatus status = asynSuccess;

  // Ql offset setpoint
  if(function == P_QL_OFFSET_SP)
  {
    cav.setOffset(value);
    recalculate = true;
    return status;
  }
  // flag to use user flattop calibration coefficients
  if(function == P_CUSTOM_COEFFS_SP)
  {
    if (!tempCustomCoeff)
    {
      lock();
      getDoubleParam(P_VC_AMP_CAL_RB, &tempVcAmpCoeff);
      getDoubleParam(P_VC_PHS_CAL_RB, &tempVcPhsCoeff);
      getDoubleParam(P_VF_AMP_CAL_RB, &tempVfAmpCoeff);
      getDoubleParam(P_VF_PHS_CAL_RB, &tempVfPhsCoeff);
      getDoubleParam(P_VR_AMP_CAL_RB, &tempVrAmpCoeff);
      getDoubleParam(P_VR_PHS_CAL_RB, &tempVrPhsCoeff);

      setDoubleParam(P_VC_AMP_CAL_SP, tempVcAmpCoeff);
      setDoubleParam(P_VC_PHS_CAL_SP, tempVcPhsCoeff);
      setDoubleParam(P_VF_AMP_CAL_SP, tempVfAmpCoeff);
      setDoubleParam(P_VF_PHS_CAL_SP, tempVfPhsCoeff);
      setDoubleParam(P_VR_AMP_CAL_SP, tempVrAmpCoeff);
      setDoubleParam(P_VR_PHS_CAL_SP, tempVrPhsCoeff);
      unlock();
    }
    tempCustomCoeff = bool(value);
    cav.userCoeffs = tempCustomCoeff;
    recalculate = true;
    return status;
  }
  // custom flattop calibration start
  if(function == P_CUSTOM_CALIBRATE_SP)
  {
    cav.vcScale = tempVcAmpCoeff;
    cav.vcRot = tempVcPhsCoeff;
    cav.vfScale = tempVfAmpCoeff;
    cav.vfRot = tempVfPhsCoeff;
    cav.vrScale = tempVrAmpCoeff;
    cav.vrRot = tempVrPhsCoeff;
    recalculate = true;
    return status;
  }

  if(function == P_CALIBRATION_STATE_SP)
  {
    cav.vfVrCalState = bool(value);
    recalculate = true;
    return status;
  }
  return status;
}

asynStatus cavityIdenAsynPortDriver::writeFloat64Array(asynUser *pasynUser, epicsFloat64 *value, size_t nElements)
{
  int function = pasynUser->reason;
  asynStatus status = asynSuccess;

  // amplitude cavity voltage
  if(function == P_DWN0_CMP0)
  {
    cav.setAmpVc(value, nElements);
    recalculate = true;
    return status;
  }
  // amplitude forward voltage
  if(function == P_DWN5_CMP0)
  {
    cav.setAmpVf(value, nElements);
    recalculate = true;
    return status;
  }
  // amplitude reflected voltage
  if (function == P_DWN6_CMP0)
  {
    cav.setAmpVr(value, nElements);
    recalculate = true;
    return status;
  }
  // phase cavity voltage
  if(function == P_DWN0_CMP1)
  {
    cav.setPhsVc(value, nElements);
    recalculate = true;
    return status;
  }
  // phase voltage forward
  if(function == P_DWN5_CMP1)
  {
    cav.setPhsVf(value, nElements);
    recalculate = true;
    return status;
  }
  // phase voltage reflected
  if(function == P_DWN6_CMP1)
  {
    cav.setPhsVr(value, nElements);
    recalculate = true;
    return status;
  }
  // x axis cavity voltage
  if(function == P_DWN0_XAXIS)
  {
    cav.setVcX(value, nElements);
    recalculate = true;
    return status;
  }
  // x axis for voltage forward
  if(function == P_DWN5_XAXIS)
  {
    cav.setVfX(value, nElements);
    recalculate = true;
    return status;
  }
  // x axis for voltage reflected
  if(function == P_DWN6_XAXIS)
  {
    cav.setVrX(value, nElements);
    recalculate = true;
    return status;
  }
  return status;
}

void cavityIdenAsynPortDriver::generateWindows(double *amp, double *pha, size_t size, size_t *range)
{
  double *qlWin, *phaWin;
  qlWin = (double*)calloc(size, sizeof(double));
  phaWin = (double*)calloc(size, sizeof(double));
  for(size_t i = 0; i < range[1]; i++)
  {
    if(i > range[0])
    {
      qlWin[i] = amp[i];
      phaWin[i] = pha[i];
    }
  }
  doCallbacksFloat64Array(qlWin, size, P_QL_AMP_WINDOW_RB, 0);
  doCallbacksFloat64Array(phaWin, size, P_QL_PHS_WINDOW_RB, 0);
  free(qlWin);
  free(phaWin);
}

cavityIdenAsynPortDriver::~cavityIdenAsynPortDriver()
{
  iocPrint("Asyn destructor\n");
}

void cavityIdenAsynPortDriver::terminateBothThreads()
{
  epicsMutexMustLock(terminateWorkersLock);
  terminateWorkers = 1;
  epicsMutexUnlock(terminateWorkersLock);
  pthread_mutex_lock(&ackFromWorkersLock);
  do
  {
    if (ackFromWorkersCount == 1)
    {
      break;
    }
    else
    {
      pthread_cond_wait(&ackFromWorkersCond, &ackFromWorkersLock);
    }
  }
  while (1);

  pthread_mutex_unlock(&ackFromWorkersLock);
}
