#include "asynPortDriver.h"
#include "cavityModel.h"

#include <pthread.h>
#include <epicsEvent.h>


class cavityIdenAsynPortDriver: public asynPortDriver
{
public:
  cavityIdenAsynPortDriver(const char *portName, size_t samplesNum, const char *devName);
  ~cavityIdenAsynPortDriver();

  asynStatus writeFloat64Array(asynUser *pasynUser, epicsFloat64* value, size_t nElements);
  asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
  asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);

  void cavityIdenCalculate(void);
  void terminateBothThreads();
  void generateWindows(double *amp, double *pha, size_t size, size_t *range);

  double system_sampling_freq;
  double res_freq;

protected:
  // flattop calibration coefficients
  int P_VC_AMP_CAL_RB;
  int P_VC_PHS_CAL_RB;
  int P_VF_AMP_CAL_RB;
  int P_VF_PHS_CAL_RB;
  int P_VR_AMP_CAL_RB;
  int P_VR_PHS_CAL_RB;
  int P_VC_AMP_CAL_SP;
  int P_VC_PHS_CAL_SP;
  int P_VF_AMP_CAL_SP;
  int P_VF_PHS_CAL_SP;
  int P_VR_AMP_CAL_SP;
  int P_VR_PHS_CAL_SP;
  // Min cavity amp level
  int P_VC_AMP_MIN_SP;
  // cavity parameters
  int P_MAX_GRAD_RB;
  int P_DETU_DECAY_RB;
  int P_DETU_PULSE_END_RB;
  int P_QL_RB;
  int P_QL_LOG_RB;
  int P_DECAY_RB;
  // waveforms
  int P_QL_WAVE_RB;
  int P_DETU_WAVE_RB;
  int P_Q_DETU_X_RB;
  int P_VC_PHS_WAVE_RB;
  int P_VC_AMP_WAVE_RB;
  int P_VF_PHS_WAVE_RB;
  int P_VF_AMP_WAVE_RB;
  int P_VR_PHS_WAVE_RB;
  int P_VR_AMP_WAVE_RB;
  int P_VC_AMP_CAL_WAVE_RB;
  int P_VC_PHS_CAL_WAVE_RB;
  int P_VF_AMP_CAL_WAVE_RB;
  int P_VF_PHS_CAL_WAVE_RB;
  int P_VR_AMP_CAL_WAVE_RB;
  int P_VR_PHS_CAL_WAVE_RB;
  // waveforms from LLRF IOC
  int P_DWN0_CMP0;
  int P_DWN5_CMP0;
  int P_DWN6_CMP0;
  int P_DWN0_CMP1;
  int P_DWN5_CMP1;
  int P_DWN6_CMP1;
  int P_DWN0_XAXIS;
  int P_DWN5_XAXIS;
  int P_DWN6_XAXIS;
  // system info from LLRF IOC
  int P_SYS_FREQ;
  int P_SYS_SAMP_FREQ;
  int P_RF_END;
  // window
  int P_QL_AMP_WINDOW_RB;
  int P_QL_PHS_WINDOW_RB;
  // offset
  int P_QL_OFFSET_RB;
  int P_QL_OFFSET_SP;
  // custom flattop calibration
  int P_CUSTOM_COEFFS_SP;
  int P_CUSTOM_CALIBRATE_SP;
  // beam params from LLRF IOC
  int P_BEAM_START;
  int P_BEAM_END;
  int P_PULSE_WIDTH;
  // Vf Vr calibration
  int P_CALIBRATION_STATE_SP;
  // error messages
  int P_ERROR_MSG_RB;

  cavityModel cav;

  bool recalculate;

  bool tempCustomCoeff;
  double tempVcAmpCoeff;
  double tempVcPhsCoeff;
  double tempVfAmpCoeff;
  double tempVfPhsCoeff;
  double tempVrAmpCoeff;
  double tempVrPhsCoeff;

  double ampMinLevel;

private:
  int terminateWorkers;

  epicsMutexId terminateWorkersLock;

  int ackFromWorkersCount;
  pthread_mutex_t ackFromWorkersLock;
  pthread_cond_t ackFromWorkersCond;
  pthread_cond_t ackForThread;
};
