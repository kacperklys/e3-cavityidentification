#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>

#include <iocsh.h>
#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <epicsExport.h>
#include "errlog.h"

#include "cavityIdenAsynPortDriver.h"


extern "C" {

  cavityIdenAsynPortDriver *drv;

  void exitFunc(void*)
  {
    drv->terminateBothThreads();
    delete drv;
  }

  int cavityIdenAsynPortDriverConfigure(const char *portName, size_t samplesNum, const char *devName)
  {
    static int onlyOnce = 0;

    if (onlyOnce) return asynSuccess;
    onlyOnce = 1;

    epicsAtExit(exitFunc, 0);

    drv = new cavityIdenAsynPortDriver(portName, samplesNum, devName);
    return(asynSuccess);
  }

  /* EPICS iocsh shell commands */
  static const iocshArg initArg0 = { "portName",iocshArgString};
  static const iocshArg initArg1 = { "max points",iocshArgInt};
  static const iocshArg initArg2 = { "dev name", iocshArgString};
  static const iocshArg * const initArgs[] = {&initArg0, &initArg1, &initArg2};

  static const iocshFuncDef initFuncDef = {"cavityIdenAsynPortDriverConfigure",3,initArgs};

  static void initCallFunc(const iocshArgBuf *args)
  {
    cavityIdenAsynPortDriverConfigure(args[0].sval, args[1].ival, args[2].sval);
  }

  void cavityIdenAsynPortDriverRegister(void)
  {
    iocshRegister(&initFuncDef,initCallFunc);
  }

  epicsExportRegistrar(cavityIdenAsynPortDriverRegister);
}

extern "C" {
  int getTimeStamp(char* time, int size)
  {
    if (!size)
    {
      return 0;
    }
    struct timeval tv;
    time_t t;
    struct tm *info;
    gettimeofday(&tv, NULL);
    int millisec;
    millisec = lrint(tv.tv_usec/1000.0);
    if (millisec >= 1000)
    {
      millisec -= 1000;
      tv.tv_sec++;
    }
    t = tv.tv_sec;
    info = localtime(&t);
    char buffer[64];
    strftime(buffer, sizeof buffer, "%d-%m-%Y %H:%M:%S", info);
    strncpy(time, buffer, size);
    return millisec;
  }

  void iocPrint(const char* format, ...)
  {
    char formatted[256] = {0};
    va_list args;
    va_start(args, format);
    {
      vsprintf(formatted, format, args);
    }
    va_end(args);
    char buffer[64];
    int millisec;
    millisec = getTimeStamp(buffer, sizeof(buffer)/sizeof(buffer[0]));
    errlogPrintf("%s.%03d ioc %s", buffer, millisec, formatted);
  }
}
