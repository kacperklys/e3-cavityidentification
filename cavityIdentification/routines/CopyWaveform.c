#include <dbDefs.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <epicsExport.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


static long CopyWaveform(aSubRecord *prec)
{
	prec->neva = prec->noa;
	prec->nevb = prec->nob;
	prec->nevc = prec->noc;
	prec->nevd = prec->nod;
	prec->neve = prec->noe;
	prec->nevf = prec->nof;
	prec->nevg = prec->nog;
	prec->nevh = prec->noh;
	prec->nevi = prec->noi;
	memcpy(prec->vala, prec->a, prec->nova*sizeof(double));
	memcpy(prec->valb, prec->b, prec->novb*sizeof(double));
	memcpy(prec->valc, prec->c, prec->novc*sizeof(double));
	memcpy(prec->vald, prec->d, prec->novd*sizeof(double));
	memcpy(prec->vale, prec->e, prec->nove*sizeof(double));
	memcpy(prec->valf, prec->f, prec->novf*sizeof(double));
	memcpy(prec->valg, prec->g, prec->novg*sizeof(double));
	memcpy(prec->valh, prec->h, prec->novh*sizeof(double));
	memcpy(prec->vali, prec->i, prec->novi*sizeof(double));
	return 0;
}

epicsRegisterFunction(CopyWaveform);

