#include <stdio.h>
#include <iostream>

using namespace std;

#include "cavityModel.h"


void cavityModel::setAmpVc(epicsFloat64 *ampVc, size_t len)
{
#ifdef DEBUG
  logger("cavityModel - setAmp\n");
#endif
  if(len > downMaxSize)
  {
    len = downMaxSize;
  }
  double sum = 0.0;
  for(size_t i = 0; i < len; i++)
  {
    vcAmpRaw[i] = ampVc[i];
    sum += ampVc[i];
  }
  vcAmpRawAvg = sum / len;
}

void cavityModel::setPhsVc(epicsFloat64 *angVc, size_t len)
{
#ifdef DEBUG
  logger("cavityModel - setPhsVc\n");
#endif
  if(len > downMaxSize)
  {
    len = downMaxSize;
  }
  for(size_t i = 0; i < len; i++)
  {
    vcPhsRaw[i] = M_PI/180 * angVc[i];
  }
}

void cavityModel::setAmpVf(epicsFloat64 *ampVf, size_t len)
{
#ifdef DEBUG
  logger("cavityModel - setAmpVf\n");
#endif
  if(len > downMaxSize)
  {
    copy(ampVf, ampVf + downMaxSize, vfAmpRaw);
  }
  else
  {
    copy(ampVf, ampVf + len, vfAmpRaw);
  }
}

void cavityModel::setPhsVf(epicsFloat64 *angVf, size_t len)
{
#ifdef DEBUG
  logger("cavityModel - setPhsVf\n");
#endif
  if(len > downMaxSize)
  {
    len = downMaxSize;
  }
  for(size_t i = 0; i < len; i++)
  {
    vfPhsRaw[i] = M_PI/180 * angVf[i];
  }
}

void cavityModel::setAmpVr(epicsFloat64 *ampVr, size_t len)
{
#ifdef DEBUG
  logger("cavityModel - setAmpVr\n");
#endif
  if(len > downMaxSize)
  {
    copy(ampVr, ampVr + downMaxSize, vrAmpRaw);
  }
  else
  {
    copy(ampVr, ampVr + len, vrAmpRaw);
  }
}

void cavityModel::setPhsVr(epicsFloat64 *angVr, size_t len)
{
#ifdef DEBUG
  logger("cavityModel - setPhsVr\n");
#endif
  if(len > downMaxSize)
  {
    len = downMaxSize;
  }
  for(size_t i = 0; i < len; i++)
  {
    vrPhsRaw[i] = M_PI/180 * angVr[i];
  }
}

void cavityModel::setVcX(epicsFloat64 *x, size_t len)
{
#ifdef DEBUG
  logger("cavityModel - setVcX\n");
#endif
  if(len > downMaxSize)
  {
    copy(x, x + downMaxSize, vcX);
  }
  else
  {
    copy(x, x + len, vcX);
  }
}

void cavityModel::setVfX(epicsFloat64 *x, size_t len)
{
#ifdef DEBUG
  logger("cavityModel - setVfX\n");
#endif
  if(len > downMaxSize)
  {
    copy(x, x + downMaxSize, vfX);
  }
  else
  {
    copy(x, x + len, vfX);
  }
}

void cavityModel::setVrX(epicsFloat64 *x, size_t len)
{
#ifdef DEBUG
  logger("cavityModel - setVrX\n");
#endif
  if(len > downMaxSize)
  {
    copy(x, x + downMaxSize, vrX);
  }
  else
  {
    copy(x, x + len, vrX);
  }
}

void cavityModel::setOffset(int off)
{
#ifdef DEBUG
  logger("cavityModel - setOffset: %d\n", off);
#endif
  if(off >= 0)
  {
    decayOffset = double(off)/1000;
  }
}

void cavityModel::setPulseEnd(double pulse)
{
#ifdef DEBUG
  logger("cavityModel - setPulseEnd: %f\n", pulse);
#endif
  if(pulse > 0)
  {
    pulseEnd = pulse;
  }
}

void cavityModel::setResFreq(double freq)
{
#ifdef DEBUG
  logger("cavityModel - setResFreq: %f\n", freq);
#endif
  if(freq > 0)
  {
    resFreq = freq;
  }
}

void cavityModel::setBeamEnd(double end)
{
#ifdef DEBUG
  logger("cavityModel - setBeamEnd: %f\n", end);
#endif
  if (end > 0)
  {
    beamEnd = end;
  }
}

void cavityModel::setBeamStart(double start)
{
#ifdef DEBUG
  logger("cavityModel - setBeamStart: %f\n", start);
#endif
  if (start > 0)
  {
    beamStart = start;
  }
}

void cavityModel::setPulseWidth(double width)
{
#ifdef DEBUG
  logger("cavityModel - setPulseWidth: %f\n", width);
#endif
  if (width > 0)
  {
    pulseWidth = width/1000;
  }
}
