#define _USE_MATH_DEFINES

#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <math.h>
#include <numeric>
#include <functional>
#include <iterator>

#include <Eigen/Dense>

using namespace std;

#include "cavityModel.h"
#ifndef M_PI
  #define M_PI 3.14159265358979323846
#endif

const double F0 = 704.25e6;
const double W12 = M_PI * F0 / 7.5e5;
const int MOVING_AVG_WIN = 3;
const double SAMP_FREQ = 117.4e6/10;

cavityModel::cavityModel(size_t size, void(*loggerFunc)(const char *format, ...))
{
  logger = loggerFunc;
#ifdef DEBUG
  logger("cavityModel - constructor\n");
#endif
  downMaxSize = size;
  // memory allocation for waveforms
  vcAmpRaw = new double[downMaxSize];
  vcPhsRaw = new double[downMaxSize];
  vfAmpRaw = new double[downMaxSize];
  vfPhsRaw = new double[downMaxSize];
  vrAmpRaw = new double[downMaxSize];
  vrPhsRaw = new double[downMaxSize];

  vcAmp = new double[downMaxSize];
  vcPhsRad = new double[downMaxSize];
  vcPhsDeg = new double[downMaxSize];
  vfAmp = new double[downMaxSize];
  vfPhsRad = new double[downMaxSize];
  vfPhsDeg = new double[downMaxSize];
  vrAmp = new double[downMaxSize];
  vrPhsRad = new double[downMaxSize];
  vrPhsDeg = new double[downMaxSize];

  vcX = new double[downMaxSize];
  vfX = new double[downMaxSize];
  vrX = new double[downMaxSize];
  vcXsec = new double[downMaxSize];

  M.real(0);
  M.imag(0);
  N.real(0);
  N.imag(0);
  Z.real(0);
  Z.imag(0);

  vcC = new complex<double>[downMaxSize];
  vfC = new complex<double>[downMaxSize];
  vrC = new complex<double>[downMaxSize];
  detVc = new complex<double>[downMaxSize];
  vfEffC = new complex<double>[downMaxSize];
  vfCalC = new complex<double>[downMaxSize];
  vrCalC = new complex<double>[downMaxSize];

  vcAmpCal = new double[downMaxSize];
  vcPhsCalRad = new double[downMaxSize];
  vcPhsCalDeg = new double[downMaxSize];
  vfAmpCal = new double[downMaxSize];
  vfPhsCalRad = new double[downMaxSize];
  vfPhsCalDeg = new double[downMaxSize];
  vrAmpCal = new double[downMaxSize];
  vrPhsCalRad = new double[downMaxSize];
  vrPhsCalDeg = new double[downMaxSize];

  q = new double[downMaxSize];
  detu = new double[downMaxSize];

  decayOffset = 0.0;
  userCoeffs = false;
  vfVrCalState = true;
}

// This function applies moving average filter to amp signals
// Default window size is 3
void cavityModel::movingAvgAmps()
{
#ifdef DEBUG
  logger("cavityModel - movingAvgAll\n");
#endif
  movingAvg(MOVING_AVG_WIN, vcAmpRaw, vcAmp);
  movingAvg(MOVING_AVG_WIN, vfAmpRaw, vfAmp);
  movingAvg(MOVING_AVG_WIN, vrAmpRaw, vrAmp);
}

// This function applies moving average filter to phase signals
// Default window size is 3
void cavityModel::movingAvgPhs()
{
#ifdef DEBUG
  logger("cavityModel - movingAvgPhs\n");
#endif
  movingAvg(MOVING_AVG_WIN, vcPhsRad, vcPhsRad);
  movingAvg(MOVING_AVG_WIN, vfPhsRad, vfPhsRad);
  movingAvg(MOVING_AVG_WIN, vrPhsRad, vrPhsRad);
}

// This function calculate moving average
void cavityModel::movingAvg(size_t window, double *inp, double *out)
{
#ifdef DEBUG
  logger("cavityModel - movingAvg\n");
#endif
  double tempOut[downMaxSize];
  tempOut[0] = inp[0] / window;
  tempOut[1] = (inp[0] + inp[1]) / window;
  for (size_t i = 2; i < downMaxSize; i++)
  {
    tempOut[i] = (inp[i-2] + inp[i-1] + inp[i]) / window;
  }
  for (size_t i = 0; i < downMaxSize; i++)
  {
    out[i] = tempOut[i];
  }
}

// This function unwraps phase
void cavityModel::unwrapArray(double *in, double *out)
{
#ifdef DEBUG
  logger("cavityModel - unwrapArray\n");
#endif
  out[0] = in[0];
  for (size_t i = 1; i < downMaxSize; i++)
  {
    double diff = in[i] - in[i-1];
    if (diff > M_PI)
    {
      out[i] = out[i-1] + diff - 2*M_PI;
    }
    else if (diff < -M_PI)
    {
      out[i] = out[i-1] + diff + 2*M_PI;
    }
    else if (fabs(diff) == M_PI)
    {
      out[i] = out[i-1] + (diff > 0 ? - 2*M_PI: 2*M_PI);
    }
    else
    {
      out[i] = out[i-1] + diff;
    }
  }
}

// This function unwraps all phase signals received
// from LLRF IOC
void cavityModel::unwrapPhaseWaves()
{
#ifdef DEBUG
  logger("cavityModel - unwrapPhaseWaves\n");
#endif
  unwrapArray(vcPhsRaw, vcPhsRad);
  unwrapArray(vfPhsRaw, vfPhsRad);
  unwrapArray(vrPhsRaw, vrPhsRad);
}

// This function unwraps calibrated phase signals
void cavityModel::unwrapPhaseCalWaves()
{
#ifdef DEBUG
  logger("cavityModel - unwrapPhaseCalWaves\n");
#endif
  unwrapArray(vfPhsCalRad, vfPhsCalRad);
  unwrapArray(vrPhsCalRad, vrPhsCalRad);
}

// This function converts time in ms from cavity voltage signal
// to seconds
void cavityModel::msToSec()
{
#ifdef DEBUG
  logger("cavityModel - msToSec\n");
#endif
  for(size_t i = 0; i < downMaxSize; i++)
  {
    vcXsec[i] = vcX[i] / 1000;
  }
}

// This function converts rad waveforms to degrees
// before calibration
void cavityModel::radToDeg()
{
#ifdef DEBUG
  logger("cavityModel - radToDeg\n");
#endif
  for(size_t i = 0; i < downMaxSize; i++)
  {
    vcPhsCalDeg[i] = 180/M_PI * vcPhsCalRad[i];
    vcPhsDeg[i] = 180/M_PI * vcPhsRad[i];
    vfPhsDeg[i] = 180/M_PI * vfPhsRad[i];
    vrPhsDeg[i] = 180/M_PI * vrPhsRad[i];
  }
}

// This functions converts rad waveforms to degrees after calibration
void cavityModel::radToDegCal()
{
#ifdef DEBUG
  logger("cavityModel - radToDegCal\n");
#endif
  for(size_t i = 0; i < downMaxSize; i++)
  {
    vfPhsCalDeg[i] = 180/M_PI * vfPhsCalRad[i];
    vrPhsCalDeg[i] = 180/M_PI * vrPhsCalRad[i];
  }
}

// This function calculates I and Q for all signals
// to compute in complex domain
void cavityModel::calculateIandQ()
{
#ifdef DEBUG
  logger("cavityModel - calculateIandQ\n");
#endif
  for(size_t i = 0; i < downMaxSize; i++)
  {
    // I = A * cos(alfa); -> real
    // Q = A * sin(alfa); -> imag
    vcC[i] = complex<double> (vcAmpCal[i] * cos(vcPhsCalRad[i]), vcAmpCal[i] * sin(vcPhsCalRad[i]));
    vfC[i] = complex<double> (vfAmp[i] * cos(vfPhsRad[i]), vfAmp[i] * sin(vfPhsRad[i]));
    vrC[i] = complex<double> (vrAmp[i] * cos(vrPhsRad[i]), vrAmp[i] * sin(vrPhsRad[i]));
  }
}

// This functions prepares raw data for the futher computations
// It normalises phase, converts it to degrees and it performs
// flattop calibration of all( vc, vf, vr) signals and
// it calculates I/Q values for all signals and derivative of cavity
// voltage signal
void cavityModel::init()
{
#ifdef DEBUG
  logger("cavityModel - initialisation\n");
#endif
  movingAvgAmps();
  unwrapPhaseWaves();
  movingAvgPhs();
  flattopWavesCalibration();
  radToDeg();
  calculateIandQ();
  calcVccDerivative();
  msToSec();
}

// This function computes calibration coefficients
// needed to compute calibrated vf and vr
void cavityModel::calculateCaliCoeffs()
{
#ifdef DEBUG
  logger("cavityModel - calculateCaluCoeffs\n");
#endif
  B = Z * A;
  C = M - A;
  D = N - B;
}

// This function compute Z factor needed for vf and vr
// calibration from mean of vf and vr signals during decay
// Z = b/a = mean(-Vf,mea/Vr,mea | decay)
void cavityModel::calculateZcoeff()
{
#ifdef DEBUG
  logger("cavityModel - calculateZcoeff\n");
#endif
  size_t idxs[2];
  double amps[2];
  findTimeDecay(idxs, amps);
  if (idxs[1] < idxs[0])
  {
    return;
  }
  complex <double> meanZ;
  size_t n = idxs[1] - idxs[0];
  for (size_t i = idxs[0]; i < idxs[1]; i++)
  {
    meanZ += -vfC[i] / vrC[i];
  }
  Z = meanZ / (n * 1.0);
}

// This function calculate effective vf
// using Z factor according to the equation below
// Vf_e = Vf + Z * Vr
void cavityModel::calculateEffVf()
{
#ifdef DEBUG
  logger("cavityModel - calculateEffVf\n");
#endif
  for (size_t i = 0; i < downMaxSize; i++)
  {
    vfEffC[i] = vfC[i] + Z * vrC[i];
  }
}

// This function converts from I/Q to amplitude and phase
// Amp^2 = I^2 + Q^2
// Phase = arctan(Q/I)
void cavityModel::IandQtoAmpAndPhase()
{
#ifdef DEBUG
  logger("cavityModel - IandQtoAmpAndPhase\n");
#endif
  for(size_t i = 0; i < downMaxSize; i++)
  {
    vfAmpCal[i] = sqrt(pow(vfCalC[i].real(), 2) + pow(vfCalC[i].imag(), 2));
    vfPhsCalRad[i] = atan(vfCalC[i].imag()/vfCalC[i].real());
    vrAmpCal[i] = sqrt(pow(vrCalC[i].real(), 2) + pow(vrCalC[i].imag(), 2));
    vrPhsCalRad[i] = atan(vrCalC[i].imag()/vrCalC[i].real());
  }
}

// This function calibrates vf and vr signals
// using computed calibration coefficients
void cavityModel::performCalibration()
{
#ifdef DEBUG
  logger("cavityModel - performCalibration\n");
#endif
  for(size_t i = 0; i < downMaxSize; i++)
  {
    vfCalC[i] = A * vfC[i] + B * vrC[i];
    vrCalC[i] = C * vfC[i] + D * vrC[i];
  }
}

// This function performs calibration of vf and vr signals:
// Vc_cali = Vf_cali + Vr_cali
// Vf_cali = a * Vf_mea + b * Vr_mea
// Vr_cali = c * Vf_mea + d * Vr_mea
// Vc_cali = m * Vf_mea + n * Vr_mea
// Z = b/a -> mean(-Vf_mea/vr_mea | decay)
// Vf_e = Vf_mea + Z * Vr_mea
// detVc_cal + (w1/2 - jdeltaw) * Vc_cal = 2 * w1/2 * Vf_cal
// detVc_cal = 2 * w1/2  * a(Vfor_meas + Z * Vref_meas)
// detVc_cal / (2 * w1/2) = a(Vfor_meas + Z * Vref_meas)
// detVc_cal / (2 * w1/2) = a * Vf_eff -> get a factor by fitting linear function
// m and n we can get using linear regression
// having a, Z, m, n we can calculate rest of the coefficients
// finally from I/Q to get calibrated vf, vr amplitudes and phases
void cavityModel::calibrateVfandVr(double detu)
{
#ifdef DEBUG
  logger("cavityModel - calibrateVfandVr\n");
#endif
  getNandMcalibrationCoeffs();
  calculateZcoeff();
  calculateEffVf();
  getAcalibrationCoef(detu);
  calculateCaliCoeffs();
  performCalibration();
  IandQtoAmpAndPhase();
  radToDegCal();
}

// This function calculate M and N calibration coefficients
// using linear regression
//    X = [Vf, Vr]
//    Y = Vc
//    Vc_cali = M * Vfor_meas + N * Vref_meas
//    matrix res           matrix X                      matrix Y
// | Vc_cali(t1) |    | Vfor_meas(t1), Vref_meas(t1) |    | M |
// | Vc_cali(t2) | =  | Vfor_meas(t2), Vref_meas(t2) | *  | N |
// | Vc_cali(t3) |    | Vfor_meas(t3), Vref_meas(t3) |
void cavityModel::getNandMcalibrationCoeffs()
{
#ifdef DEBUG
  logger("cavityModel - getNandMcalibrationCoeffs\n");
#endif
  Eigen::MatrixXcd X(downMaxSize, 2);
  Eigen::VectorXcd Y(downMaxSize);
  Eigen::VectorXcd res(2);
  X = Eigen::Map<Eigen::MatrixXcd>(vfC, downMaxSize, 1);
  X = Eigen::Map<Eigen::MatrixXcd>(vrC, downMaxSize, 2);
  Y = Eigen::Map<Eigen::VectorXcd>(vcC, downMaxSize);
  res = X.colPivHouseholderQr().solve(Y);
  complex<double> *c = res.data();
  M = c[1];
  N = c[0];
}

// This function computes a calibration coefficient
// detVc_cal + (w1/2 - jdeltaw) * Vc_cal = 2 * w1/2 * Vf_cal
// detVc_cal = 2 * w1/2  * a(Vfor_meas + Z * Vref_meas)
// detVc_cal / (2 * w1/2) = a(Vfor_meas + Z * Vref_meas)
// detVc_cal / (2 * w1/2) = a * Vf_eff -> get a factor by fitting linear function
// this is calculateed during decay
void cavityModel::getAcalibrationCoef(double detuRad)
{
#ifdef DEBUG
  logger("cavityModel - getAcalibrationCoef\n");
#endif
  size_t tIdx = 0;
  double timeCloseEndPulse = 0.0;
  timeCloseEndPulse = findClosest(vcX, pulseEnd, &tIdx, 0, downMaxSize);
  size_t n = 900;
  complex<double> tmpVf[n];
  complex<double> detuC(0, detuRad);
  // using cavity equation and some assumptions that appears duringdecay
  for(size_t i = 0; i < n; i++)
  {
    tmpVf[i] = (detVc[i + tIdx - 1000] + (W12 - detuC) * vcC[i + tIdx - 1000]) / (2 * W12 * vfEffC[i + tIdx - 1000]);
  }
  // Create matrix placeholder of size n * k, k = order of polynominal, n = number of points
  size_t order = 1;
  Eigen::MatrixXcd time(n, order + 1);
  Eigen::VectorXcd val(n);
  Eigen::VectorXcd res;
  val = Eigen::Map<Eigen::VectorXcd>(tmpVf, n);
  // Populate the matrix
  for(size_t i = 0; i < n; i++)
  {
    for(size_t j = 0; j < order + 1; j++)
    {
      time(i, j) = pow(vcXsec[i + tIdx - 1000], j);
    }
  }
  // Solve for linear least square fit
  res = time.householderQr().solve(val);
  complex<double> *c = res.data();
  complex<double> fitted = c[0] + c[1] * (timeCloseEndPulse/1000);
  A.real(fitted.real());
  A.imag(fitted.imag());
}

// This function performs amplitude normalisation
// using factors found from flattop area
void cavityModel::calibrateAmpWaves()
{
#ifdef DEBUG
  logger("cavityModel - calibrateAmpWaves\n");
#endif
  for (size_t i = 0; i < downMaxSize; i++)
  {
    vcAmpCal[i] = vcAmp[i] * vcScale;
    vfAmp[i] *= vfScale;
    vrAmp[i] *= vrScale;
  }
}

// This function performs phase normalisation
// using factors found from flattop area
void cavityModel::calibratePhaseWaves()
{
#ifdef DEBUG
  logger("cavityModel - calibratePhaseWaves\n");
#endif
  for (size_t i = 0; i < downMaxSize; i++)
  {
    vcPhsCalRad[i] = vcPhsRad[i]; // - vcRot;
    vfPhsRad[i] -= vfRot;
    vrPhsRad[i] -= vrRot;
  }
}

// This function performs all signals normalisation
// It finds factors using flattop area
// and then multiplies all singals by them
void cavityModel::flattopWavesCalibration()
{
#ifdef DEBUG
  logger("cavityModel - flattopWavesCalibration\n");
#endif
  if (!userCoeffs)
  {
    findFlattopCalibration(&vcScale, &vcRot, &vfScale, &vfRot, &vrScale, &vrRot);
  }
  calibrateAmpWaves();
  calibratePhaseWaves();
}

// This function finds factors for signals normalisation
// The factors are found on flattop area of all signals
void cavityModel::findFlattopCalibration(double *ampVcAvg, double *angVcAvg, double *ampVfAvg, double *angVfAvg, double *ampVrAvg, double *angVrAvg)
{
#ifdef DEBUG
  logger("cavityModel - flattopCalibration\n");
#endif
  size_t pulseStartIdx = 0;
  findClosest(vcX, pulseEnd - 0.2, &pulseStartIdx, 0, downMaxSize);

  size_t pulseEndIdx = 0;
  findClosest(vcX, pulseEnd, &pulseEndIdx, 0, downMaxSize);

  double ampVcSum = 0.0, angVcSum = 0.0, ampVfSum = 0.0, angVfSum = 0.0, ampVrSum = 0.0, angVrSum = 0.0;
  for (size_t i = pulseStartIdx; i < pulseEndIdx; i++)
  {
    ampVcSum += vcAmp[i];
    angVcSum += vcPhsRad[i];

    ampVfSum += vfAmp[i];
    angVfSum += vfPhsRad[i];

    ampVrSum += vrAmp[i];
    angVrSum += vrPhsRad[i];
  }

  *ampVcAvg = 1/(ampVcSum/(pulseEndIdx - pulseStartIdx));
  *ampVfAvg = 1/(ampVfSum/(pulseEndIdx - pulseStartIdx));
  *ampVrAvg = 1/(ampVrSum/(pulseEndIdx - pulseStartIdx));
  *angVcAvg = angVcSum/(pulseEndIdx - pulseStartIdx);
  *angVfAvg = angVfSum/(pulseEndIdx - pulseStartIdx);
  *angVrAvg = angVrSum/(pulseEndIdx - pulseStartIdx);
}

// This function calculates derivative of voltage cavity signal
void cavityModel::calcVccDerivative()
{
#ifdef DEBUG
  logger("cavityModel - calcVccDerivative\n");
#endif
  adjacent_difference(vcC, vcC + downMaxSize - 1, detVc);
}

// This function finds max gradient value of cavity voltage signal
double cavityModel::maxGradient()
{
#ifdef DEBUG
  logger("cavityModel - maxGradient\n");
#endif
  double max = vcAmp[0];
  for(size_t i = 0; i < downMaxSize; i++)
  {
    if(vcAmpCal[i] > max)
    {
      max = vcAmpCal[i];
    }
  }
  return max;
}

// This functions finds the closest value in the specific range of array
double cavityModel::findClosest(double *arr, double target, size_t *idx, size_t start, size_t end)
{
#ifdef DEBUG
  logger("cavityModel - findClosest\n");
#endif
  double res = arr[0];
  for(size_t i = start; i < end; i++)
  {
    if(abs(target - res) > abs(target - arr[i]))
    {
      res = arr[i];
      *idx = i;
    }
  }
  return res;
}

// This function finds array indexes and time values for
// voltage cavity decay, it finds time between value in rf end pulse and
// rf end pulse * 0.37
double cavityModel::findTimeDecay(size_t *indexes, double *amps)
{
#ifdef DEBUG
  logger("cavityModel - findTimeDecay\n");
#endif
  size_t tIdx = 0, ampIdx = 0;
  double timeCloseEndPulse = 0.0;
  // find the index of the closest value to the pulse end (from LLRF IOC) on the  x axis
  timeCloseEndPulse = findClosest(vcX, pulseEnd + decayOffset, &tIdx, 0, downMaxSize);
  // check index range
  if(tIdx > downMaxSize)
  {
    tIdx = downMaxSize;
  }
  // calculate 0.37 of the amplitude value in the end pulse point
  amps[0] = vcAmp[tIdx];
  double newTarget = vcAmp[tIdx] * 0.37;
  // find the index of the closest value to the 0.37 of the amplitude
  amps[1] = findClosest(vcAmp, newTarget, &ampIdx, tIdx, downMaxSize);
  // copy indexes to array
  indexes[0] = tIdx;
  indexes[1] = ampIdx;
  return vcX[ampIdx] - timeCloseEndPulse;
}

// The function find slope factor of the following equation:
// ln(vc) = ln(vc0) -w1/2 * t during decay
// where w1/2 is used later to calculate Q factor
double cavityModel::findLogarithmSlope(size_t *indexes)
{
#ifdef DEBUG
  logger("cavityModel - findLogarithmSlope\n");
#endif
  if(indexes[1] > indexes[0])
  {
    size_t n = indexes[1] - indexes[0];
    double logAmp[n];
    // calculate logarithm
    for(size_t i = 0; i < n; i++)
    {
      logAmp[i] = log(vcAmp[i + indexes[0]]);
    }
    // find slope of the logarithm
    size_t order = 1;
    Eigen::MatrixXd time(n, order + 1);
    Eigen::VectorXd val(n);
    Eigen::VectorXd res;
    val = Eigen::Map<Eigen::VectorXd>(logAmp, n);
    // Populate the matrix
    for(size_t i = 0; i < n; i++)
    {
      for(size_t j = 0; j < order + 1; j++)
      {
        time(i, j) = pow(vcXsec[i + indexes[0]], j);
      }
    }
    // Solve for linear least square fit
    res = time.householderQr().solve(val);
    double *c = res.data();
    return -c[1];
  }
  return 0.0;
}

// This functions find slope factor of the following equation
// vcphase = vcphase0 + detu * t
// the slope factor equals to detuning
double cavityModel::findDetuningSlope(size_t *indexes)
{
#ifdef DEBUG
  logger("cavityModel - findDetuningSlope\n");
#endif
  if(indexes[1] > indexes[0])
  {
    size_t n = indexes[1] - indexes[0];
    size_t order = 1;
    Eigen::MatrixXd time(n, order + 1);
    Eigen::VectorXd val(n);
    Eigen::VectorXd res;
    val = Eigen::Map<Eigen::VectorXd>(vcPhsRad + indexes[0], n);
    // Populate the matrix
    for(size_t i = 0; i < n; i++)
    {
      for(size_t j = 0; j < order + 1; j++)
      {
        time(i, j) = pow(vcXsec[i + indexes[0]], j);
      }
    }
    // Solve for linear least square fit
    res = time.householderQr().solve(val);
    double *c = res.data();
    return c[1];
  }
  return 0.0;
}

// This function calculates Q factor from decay
// tdecay = 2*Q/wn
// wn = 2*pi*fn
// where fn is cavity resonance frequency
double cavityModel::QlFromDecay(double decay)
{
#ifdef DEBUG
  logger("cavityModel - QlFromDecay\n");
#endif
  return decay/1000 * M_PI * F0;
}

// This functions calculates Q from logarithm slope factor
// w1/2 = wn/2Q -> Q = wn/2*w/1/2
// wn = 2*pi*fn
double cavityModel::QlFromLogarithm(double w12Ext)
{
#ifdef DEBUG
  logger("cavityModel - QlFromLogarithm\n");
#endif
  return (w12Ext != 0) ? abs((2 * M_PI * F0)/(2 * w12Ext)) : 0.0;
}

// This function calculate Q and detuning over time
// w1/2 - jdeltaw = (2 * w1/2_ext * Vfor_cali - detVc)/Vc
// where deltaw is detuning
// and w1/2 = wn/2Q -> pi * fn / w1/2
// we calulcate those values from 15us to limit peaks
void cavityModel::calculateQandDetuOverTime(double w12Ext, size_t *n, size_t *idx300us)
{
#ifdef DEBUG
  logger("cavityModel - calculateQandDetuOverTime\n");
#endif
  // from 300us to pulse end
  size_t idxPulse = 0;
  findClosest(vcX, 0.3, idx300us, 0, downMaxSize);
  findClosest(vcX, pulseEnd, &idxPulse, 0, downMaxSize);
  if(idxPulse < *idx300us)
  {
    return;
  }
  *n = idxPulse - *idx300us;
  for(size_t i = 0; i < *n; i++)
  {
    complex<double> res;
    if(vfVrCalState)
    {
      res = (2 * w12Ext * vfCalC[i + *idx300us] - detVc[i + *idx300us] * SAMP_FREQ) / vcC[i + *idx300us];
    }
    else
    {
      res = (2 * w12Ext * vfC[i + *idx300us] - detVc[i + *idx300us] * SAMP_FREQ) / vcC[i + *idx300us];
    }
    q[i] = (M_PI * F0) / res.real();
    detu[i] = -res.imag();

  }
  for(size_t i = 0; i < *n; i++)
  {
    detu[i] = detu[i] / (2 * M_PI);
  }
}

double cavityModel::calcDetuPulseEnd(size_t detuLen)
{
  size_t idxEndPulse = 0, idxBeforeEndPulse = 0;
  // find the index of the closest value to the pulse end (from LLRF IOC) on the x axis
  findClosest(vcX, pulseEnd, &idxEndPulse, 0, downMaxSize);
  // find the index of the pulse end time - 50 us 
  findClosest(vcX, pulseEnd - 0.01, &idxBeforeEndPulse, 0, downMaxSize);
  // calculate detuning average
  double avg = 0.0;
  size_t len = idxEndPulse - idxBeforeEndPulse;
  for(size_t i = detuLen - len; i <= detuLen; i++)
  {
    avg += detu[i];  
  }
  return avg / (len + 1);
}

cavityModel::~cavityModel()
{
#ifdef DEBUG
  logger("cavityModel - desctructor\n");
#endif
  // delete raw signals
  delete[] vcPhsRaw;
  delete[] vfAmpRaw;
  delete[] vfPhsRaw;
  delete[] vrAmpRaw;
  delete[] vrPhsRaw;
  // delete signals - vc, vf, vr
  delete[] vcAmp;
  delete[] vcPhsRad;
  delete[] vcPhsDeg;
  delete[] vfAmp;
  delete[] vfPhsRad;
  delete[] vfPhsDeg;
  delete[] vrAmp;
  delete[] vrPhsRad;
  delete[] vrPhsDeg;
  delete[] vcC;
  delete[] vfC;
  delete[] vrC;
  // delete calibrated vc, vf, and vr
  delete[] vrCalC;
  delete[] vfCalC;
  delete[] vcAmpCal;
  delete[] vcPhsCalRad;
  delete[] vcPhsCalDeg;
  delete[] vfAmpCal;
  delete[] vfPhsCalRad;
  delete[] vfPhsCalDeg;
  delete[] vrAmpCal;
  delete[] vrPhsCalRad;
  delete[] vrPhsCalDeg;
  // clear q and detuning over time
  delete[] q;
  delete[] detu;
  // clear x axis - time for vc, vf, vr
  delete[] vcX;
  delete[] vfX;
  delete[] vrX;
  delete[] vcXsec;
}
