#include <iostream>
#include <getopt.h>

#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>

#include "cavityModel.h"
//#include "butterFilter.h"


#define FREQ 704.420
#define PULSE_END 3.001

using namespace std;


template <typename filename, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9 >
bool writeCsvFile(filename &fileName, T1 column1, T2 column2, T3 column3, T4 column4, T5 column5, T6 column6, T7 column7, T8 column8, T9 column9)
{
  fstream file;
  file.open (fileName, ios::out | ios::app);
  if (file)
  {
    file << column1 << ",";
    file << column2 << ",";
    file << column3 << ",";
    file << column4 << ",";
    file << column5 << ",";
    file << column6 << ",";
    file << column7 << ",";
    file << column8 << ",";
    file << column9;
    file <<  std::endl;
    return true;
  }
  else
  {
    return false;
  }
}

void printLog( const char* format, ... )
{
  char formatted[256] = {0} ;
  va_list args ;
  va_start(args,format) ;
  {
    vsprintf(formatted, format, args) ;
  }
  va_end(args) ;
  printf( "app %s", formatted ) ;
}

int main(int argc, char* argv[])
{
  int opt ;
  char paths[7][256] = { {0}, {0}, {0}, {0}, {0}, {0}, {0} };
  double cav_scale = 1.0, cav_rot = 0.0, vf_scale = 0.0, vf_rot = 0.0, vr_scale = 0.0, vr_rot = 0.0;
  double offset = 0.0;
  size_t samples = 40000;

  while ((opt = getopt (argc, argv, "a:b:c:d::o:p:q:r:s:t:u:h:j:n:")) != -1)
  {
    switch(opt)
    {
    case 'a':
    {
      char *err;
      cav_scale = strtod(optarg, &err);
      if(*err != 0)
      {
        printLog("Wrong cavity scale input format\n");
        cav_scale = 0.0;
      }
      break;
    }
    case 'b':
    {
      char *err;
      cav_rot = strtod(optarg, &err);
      if(*err != 0)
      {
        printLog("Wrong cavity rotate input format\n");
        cav_rot = 0.0;
      }
      break;
    }
    case 'c':
    {
      char *err;
      vf_scale = strtod(optarg, &err);
      if(*err != 0)
      {
        printLog("Wrong vf scale input format\n");
        vf_scale = 0.0;
      }
      break;
    }
    case 'd':
    {
      char *err;
      vf_rot = strtod(optarg, &err);
      if(*err != 0)
      {
        printLog("Wrong vf rotate input format\n");
        vf_rot = 0.0;
      }
      break;
    }
    case 'e':
    {
      char *err;
      vf_scale = strtod(optarg, &err);
      if(*err != 0)
      {
        printLog("Wrong vr scale input format\n");
        vr_scale = 0.0;
      }
      break;
    }
    case 'f':
    {
      char *err;
      vr_rot = strtod(optarg, &err);
      if(*err != 0)
      {
        printLog("Wrong vr rotate input format\n");
        vr_rot = 0.0;
      }
      break;
    }
    // waveform with time
    case 'n':
    {
      ifstream f(optarg);
      if(f.good())
      {
        memcpy(paths[0], optarg, strlen(optarg));
      }
      else
      {
        printLog("File with time does not exist\n");
        return 1;
      }
      break;
    }
    // waveform with cavity response - amplitude
    case 'o':
    {
      ifstream f(optarg);
      if(f.good())
      {
        memcpy(paths[1], optarg, strlen(optarg));
      }
      else
      {
        printLog("File with cavity response amplitude does not exist\n");
        return 1;
      }
      break;
    }
    // waveform with cavity response - phase
    case 'p':
    {
      ifstream f(optarg);
      if(f.good())
      {
        memcpy(paths[2], optarg, strlen(optarg));
      }
      else
      {
        printLog("File with cavity response phase does not exist\n");
        return 1;
      }
      break;
    }
    // waveform with voltage forward - amplitude
    case 'q':
    {
      ifstream f(optarg);
      if(f.good())
      {
        memcpy(paths[3], optarg, strlen(optarg));
      }
      else
      {
        printLog("File with voltage forward amplitude does not exist\n");
        return 1;
      }
      break;
    }
    // waveform with voltage forward - phase
    case 'r':
    {
      ifstream f(optarg);
      if(f.good())
      {
        memcpy(paths[4], optarg, strlen(optarg));
      }
      else
      {
        printLog("File with voltage forward phase does not exist\n");
        return 1;
      }
      break;
    }
    // waveform with voltage reflected - amplitude
    case 't':
    {
      ifstream f(optarg);
      if(f.good())
      {
        memcpy(paths[5], optarg, strlen(optarg));
      }
      else
      {
        printLog("File with voltage reflected amplitude does not exist\n");
        return 1;
      }
      break;
    }
    // waveform with voltage reflected - phase
    case 'u':
    {
      ifstream f(optarg);
      if(f.good())
      {
        memcpy(paths[6], optarg, strlen(optarg));
      }
      else
      {
        printLog("File with voltage reflected phase does not exist\n");
        return 1;
      }
      break;
    }
    case 's':
    {
      size_t t;
      t = atoi(optarg);
      if(t != 0)
      {
        samples = t;
      }
      else
      {
        printLog("Wrong samples input format\n");
        return 1;
      }
    }
    case 'j':
    {
      char *err;
      offset = strtod(optarg, &err);
      if(*err != 0)
      {
        printLog("Wrong offset\n");
        return 1;
      }
      break;
    }
    }
  }
  // check paths
  for(size_t i = 0; i < sizeof(paths)/sizeof(paths[0]); i++)
  {
    if(strlen(paths[i]) == 0)
    {
      printLog("Missing waveform data\n");
      return 1;
    }
  }

  cavityModel cav(samples, printLog);

  vector<vector<double> > waveforms;

  for(size_t i = 0; i < sizeof(paths)/sizeof(paths[0]); i++)
  {
    ifstream f(paths[i]);
    string line;
    while(getline(f,line))
    {
      stringstream lineStream(line);
      double value;
      vector<double> values;
      while(lineStream >> value)
      {
        values.push_back(value);
      }
      waveforms.push_back(values);
    }
  }
  // load waveform to cavity class
  double *time = &waveforms[0][0];
  double *amp = &waveforms[1][0], *ang = &waveforms[2][0];
  double *ampVf = &waveforms[3][0], *angVf = &waveforms[4][0];
  double *ampVr = &waveforms[5][0], *angVr = &waveforms[6][0];
  // load waveforms
  cav.setAmpVc(amp, samples);
  cav.setPhsVc(ang, samples);
  cav.setAmpVf(ampVf, samples);
  cav.setPhsVf(angVf, samples);
  cav.setAmpVr(ampVr, samples);
  cav.setPhsVr(angVr, samples);
  cav.setVcX(time, samples);
  // custom calibration
  cav.vcScale = cav_scale;
  cav.vcRot = cav_rot;
  cav.vfScale = vf_scale;
  cav.vfRot = vf_rot;
  cav.vrScale = vr_scale;
  cav.vrRot = vr_rot;
  // load configuration
  cav.setOffset(offset);
  cav.setResFreq(FREQ);
  cav.setPulseEnd(PULSE_END);
  // process signals
  cav.init();
  // find decay of cavity voltage
  size_t idxs[2];
  double amps[2];
  double timeDecay = cav.findTimeDecay(idxs, amps);
  // caluclate Q from decay
  double qlDecay = cav.QlFromDecay(timeDecay);
  // calculate detuning
  double alfaDetu = cav.findDetuningSlope(idxs);
  // calculate Q from logarithm decay slope
  double w12Ext = cav.findLogarithmSlope(idxs);
  // calibrate cavity forward and cavity reflected signals
  cav.calibrateVfandVr(alfaDetu);
  // find max gradient
  double max = cav.maxGradient();
  // calculate detuning and Q over time using
  size_t n = 0, idx300us = 0;
  cav.calculateQandDetuOverTime(w12Ext, &n, &idx300us);
  printLog("Ql from time decay: %f\n", qlDecay);
  printLog("Ql from slope of the logarithm %f\n", cav.QlFromLogarithm(w12Ext));
  printLog("Max gradient: %f\n", max);
  printLog("Detuning: %f\n", alfaDetu/(2*M_PI));
  // create file with waveforms
  std::ofstream outFile("waveforms.csv");
  if (!outFile.is_open())
  {
    printLog("Error opening file\n");
    return 1;
  }
  outFile << "pAmp,pPha,pAmpVf,pPhaVf,pAmpVr,pPhaVr,pulseQl,pulseDet,Time\n";
  for(size_t i = 0; i < cav.downMaxSize; i++)
  {
    outFile << cav.vcAmpCal[i] << "," << cav.vcPhsCalDeg[i] << "," << cav.vfAmp[i] << "," << cav.vfPhsDeg[i] << "," << cav.vrAmp[i] << "," << cav.vrPhsDeg[i] << "," << cav.q[i] << "," << cav.detu[i] << "," << time[i] << "\n";
  }
  outFile.close();
  printLog("Data written to waveforms.csv successfully\n");
  return 0;
}
