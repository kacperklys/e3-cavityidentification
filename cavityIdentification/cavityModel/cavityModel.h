#ifdef STANDALONE
  typedef double epicsFloat64;
  typedef int epicsInt32;
#else
  #include <epicsTypes.h>
#endif

#include <complex>
#include <vector>

using namespace std;


class cavityModel
{
public:
  cavityModel(size_t size, void (*loggerFunc)(const char *format, ...));
  ~cavityModel();

  void init();
  void calcVccDerivative();

  void calculateQandDetuOverTime(double w12Ext, size_t *n, size_t *idx15us);
  void calibrateVfandVr(double detu);

  void setAmpVc(epicsFloat64 *ampVc, size_t len);
  void setAmpVf(epicsFloat64 *ampVf, size_t len);
  void setAmpVr(epicsFloat64 *angVr, size_t len);
  void setPhsVc(epicsFloat64 *angVc, size_t len);
  void setPhsVf(epicsFloat64 *angVf, size_t len);
  void setPhsVr(epicsFloat64 *angVr, size_t len);

  void setVcX(epicsFloat64 *x, size_t len);
  void setVfX(epicsFloat64 *x, size_t len);
  void setVrX(epicsFloat64 *x, size_t len);

  double findClosest(double *arr, double target, size_t *idx, size_t start, size_t end);

  double maxGradient();

  void setOffset(int off);

  void setPulseWidth(double width);
  void setPulseEnd(double pulse);
  void setBeamStart(double start);
  void setBeamEnd(double end);
  void setResFreq(double freq);

  double findTimeDecay(size_t *indexes, double *amps);
  double findLogarithmSlope(size_t *indexes);
  double findDetuningSlope(size_t *indexes);

  double QlFromDecay(double decay);
  double QlFromLogarithm(double w12Ext);

  double calcDetuPulseEnd(size_t detuLen);

  double vcScale;
  double vfScale;
  double vrScale;
  double vcRot;
  double vfRot;
  double vrRot;
  bool userCoeffs;

  double* vcAmpCal;
  double* vcPhsCalRad;
  double* vcPhsCalDeg;
  double* vfAmpCal;
  double* vfPhsCalRad;
  double* vfPhsCalDeg;
  double* vrAmpCal;
  double* vrPhsCalRad;
  double* vrPhsCalDeg;
  double* q;
  double* detu;

  bool vfVrCalState;

  size_t downMaxSize;

  double *vcAmp;
  double *vcPhsDeg;
  double *vfAmp;
  double *vfPhsDeg;
  double *vrAmp;
  double *vrPhsDeg;
  double *vcX;
  double *vfX;
  double *vrX;

  double vcAmpRawAvg;

private:
  void (*logger)(const char *format, ...);

  double decayOffset;
  double pulseEnd;
  double pulseWidth;
  double resFreq;
  double beamStart;
  double beamEnd;

  double *vcAmpRaw; // MV/m
  double *vcPhsRaw; // rad
  double *vfAmpRaw; // MV/m
  double *vfPhsRaw; // rad
  double *vrAmpRaw; // MV/m
  double *vrPhsRaw; // rad
  double *vcPhsRad; // rad
  double *vfPhsRad; // rad
  double *vrPhsRad; // rad
  double *vcXsec;   // sec

  complex<double> *vcC;
  complex<double> *vfC;
  complex<double> *vrC;
  complex<double> *detVc;
  complex<double> M;
  complex<double> N;
  complex<double> Z;
  complex<double> A;
  complex<double> B;
  complex<double> C;
  complex<double> D;

  complex<double>* vfEffC;
  complex<double>* vrCalC;
  complex<double>* vfCalC;

  void scaleMagnitudeWaves();
  void scaleAngleWave();

  void movingAvg(size_t window, double *inp, double *out);
  void movingAvgAmps();
  void movingAvgPhs();

  void calculateIandQ();
  void calculateZcoeff();
  void calculateEffVf();
  void calculateCaliCoeffs();
  void performCalibration();
  void IandQtoAmpAndPhase();
  void getNandMcalibrationCoeffs();
  void getAcalibrationCoef(double detuRad);

  void unwrapPhaseCalWaves();
  void calibrateAmpWaves();
  void calibratePhaseWaves();
  void flattopWavesCalibration();
  void findFlattopCalibration(double *ampVcAvg, double *angVcAvg, double *ampVfAvg, double *angVfAvg, double *ampVrAvg, double *angVrAvg);
  void unwrapPhaseWaves();
  void msToSec();
  void radToDeg();
  void radToDegCal();
  void unwrapArray(double *in, double *out);
};
