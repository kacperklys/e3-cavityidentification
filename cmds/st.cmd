require cavityidentification

epicsEnvSet("P", "1:")
epicsEnvSet("R", "2:")
epicsEnvSet("PORT", "port")
epicsEnvSet("DEVICE_NAME", "test")
epicsEnvSet("LLRF_PREFIX", "LLRF-010:RFS-DIG-101:")
epicsEnvSet("LLRF_SYSTEM", "LLRF-LLRF1::")
epicsEnvSet("LLRF_EVR", "LLRF:EVR:")
epicsEnvSet("DWNMAXSIZE", 40000)

iocshLoad("$(cavityidentification_DIR)/cavity.iocsh", "P=$(P),R=$(R),PORT=$(PORT),DEVICE_NAME=$(DEVICE_NAME),LLRF_PREFIX=$(LLRF_PREFIX),LLRF_SYSTEM=$(LLRF_SYSTEM),DWNMAXSIZE=$(DWNMAXSIZE),LLRF_EVR=$(LLRF_EVR)")
